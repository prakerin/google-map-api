<!DOCTYPE html>
<html>
  <head>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8SM86gKJSLXJbfFkZpVD2R43Tw7P3AjQ"> </script>

    <script>
      var btp=new google.maps.LatLng(-6.970458, 107.630314);
      function initialize(){
        var mapProp = {
          center:btp,
          zoom:13,
          mapTypeId:google.maps.MapTypeId.ROADMAP
        };
  
        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

        var myCity = new google.maps.Circle({
          center:btp,
          radius:20000,
          //Menentukan besar lingkaran
          strokeColor:"#0000FF",
          //Menentukan warna garis
          strokeOpacity:0.8,
          //Menentukan transparan garis
          strokeWeight:2,
          //Menentukan tebal garis
          fillColor:"#0000FF",
          //Menentukan warna lingkaran
          fillOpacity:0.4
          //Menentukan transparan warna
        });

        myCity.setMap(map);
      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>

  <body>
    <div id="googleMap" style="width:500px;height:380px;"></div>
  </body>
</html>
