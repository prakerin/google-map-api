<!DOCTYPE html>
<html>
	<head>
		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8SM86gKJSLXJbfFkZpVD2R43Tw7P3AjQ"> </script> 
		<!-- LOAD GoogleMap JS -->

		<script>
			var myCenter=new google.maps.LatLng(-6.970458, 107.630314);
			//Membuat variabel untuk menentukan lokasi awal.

			var marker;
			//Membuat variabel untuk menentukan letak marker

			function initialize(){ 
			//Menginisialisasi map

				var mapProp = {
  					center:myCenter,
  					//Menentukan lokasi awal map
  					zoom:18,
  					//Zoom
  					mapTypeId:google.maps.MapTypeId.HYBRID
  					//Tipe map (ROAD MAP, SATELLITE, HYBRID, TERRAIN)
  				};
  				//Fungsi untuk mendefinisikan propertis pada google map

				var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
				//Membuat objek google map 

				var marker=new google.maps.Marker({
  					position:myCenter,
  					//Menentukan titik marker
  					animation:google.maps.Animation.BOUNCE
  					//Membuat animasi pada marker
  				});
  				//Mendeklarasikan marker 

				marker.setMap(map);
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
	</head>
	
	<body>
		<div id="googleMap" style="width:500px;height:380px;"></div>
	</body>
</html>
