<!DOCTYPE html>
<html>
	<head>
		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8SM86gKJSLXJbfFkZpVD2R43Tw7P3AjQ"> </script>

		<script>
			var btp=new google.maps.LatLng(-6.970458, 107.630314);
			var telkom=new google.maps.LatLng(-6.972054, 107.631270);
			//Menentukan lokasi stavanger
			var smk=new google.maps.LatLng(-6.973330, 107.626754);
			//Menentukan lokasi amsterdam
			var mts=new google.maps.LatLng(-6.974432, 107.603092);
			//Menentukan lokasi london

			function initialize(){
				var mapProp = {
  					center:btp,
  					//Menentukan lokasi awal map
  					zoom:15,
  					//Zoom
  					mapTypeId:google.maps.MapTypeId.ROADMAP
  					//Tipe map (ROAD MAP, SATELLITE, HYBRID, TERRAIN)
  				};
  				//Fungsi untuk mendefinisikan propertis pada google map
		  
				var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
				//Membuat objek google map 

				var myTrip=[btp,telkom,smk,mts];
				//Menentukan lokasi untuk dimenarik garis
				var flightPath=new google.maps.Polyline({
		  			path:myTrip,
		  			//Lokasi untuk menarik garis
  					strokeColor:"#0000FF",
  					//Warna garis
  					strokeOpacity:0.8,
  					//Transparan garis
  					strokeWeight:2
  					//Tebal garis
  				});
  				//Fungsi untuk membuat garis

				flightPath.setMap(map);
				//Membuat garis pada map
			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
	</head>

	<body>
		<div id="googleMap" style="width:1350px;height:630px;"></div>
	</body>
</html>
