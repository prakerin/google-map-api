<!DOCTYPE html>
<html>
  <head>  
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8SM86gKJSLXJbfFkZpVD2R43Tw7P3AjQ"> </script>

    <script>
      var myCenter=new google.maps.LatLng(-6.970458, 107.630314);

      function initialize(){
        var mapProp = {
          center:myCenter,
          zoom:5,
          mapTypeId:google.maps.MapTypeId.ROADMAP
        };

        var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

        var marker=new google.maps.Marker({
          position:myCenter,
        });
        marker.setMap(map);

        var infowindow = new google.maps.InfoWindow({
          content:"Hello BTP!"
        });

        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map,marker);
        });//Membuat event click membuka infowindow
      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>

  <body>
    <div id="googleMap" style="width:500px;height:380px;"></div>
  </body>
</html>
