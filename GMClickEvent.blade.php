<!DOCTYPE html>
<html>
  <head>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyC8SM86gKJSLXJbfFkZpVD2R43Tw7P3AjQ"> </script>

    <script>
      var map;
      var myCenter=new google.maps.LatLng(-6.970458, 107.630314);

      function initialize(){
        var mapProp = {
          center:myCenter,
          zoom:13,
          mapTypeId:google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

        google.maps.event.addListener(map, 'click', function(event) {
          placeMarker(event.latLng);
        });
      }

      function placeMarker(location) {
      //Fungsi untuk menentukan letak marker
        var marker = new google.maps.Marker({
          position: location,
          map: map,
        });
        
        var infowindow = new google.maps.InfoWindow({
          content: 'Latitude: ' + location.lat() + '<br>Longitude: ' + location.lng()
          //Memberikan info latitude dan longitude saat diklik
        });
        infowindow.open(map,marker);
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>

  <body>
    <div id="googleMap" style="width:500px;height:380px;"></div>
  </body>
</html>
